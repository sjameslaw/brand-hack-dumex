function $ (id) { return document.getElementById(id); };

var step1 = $('happinessStep');
var step2 = $('happinessResult');
var vid = $('videoel');

var happyIcon = $('happyIcon');
var unhappyIcon = $('unhappyIcon');

var happinessFramesCounter = 0;
var frameLimit = 300;
var happinessValueLimit = 50;

function goToSummary () {
  window.location = '/summary';
};

/********** check and set up video/webcam **********/

function enablestart() {
  var startbutton = $('startbutton');
  startbutton.innerHTML = "DETECT HAPPINESS";
  startbutton.disabled = null;
};

function showResult(happinessValue) {
  step1.className = "step happiness";

  $("happinessValue").innerHTML = happinessValue;

  var dumexStore = JSON.parse(localStorage.getItem('Dumex'));
  if (!dumexStore.results) {
    dumexStore.results = {};
  }
  dumexStore.results.happiness = happinessValue;
  localStorage.setItem('Dumex', JSON.stringify(dumexStore));

  //IS HAPPY
  if (happinessValue > happinessValueLimit) {
    step2.className = "step happiness__results show is-happy";
  } else {
    step2.className = "step happiness__results show is-unhappy";
  }

};

function reInitDetection() {
  happinessFramesCounter = 0;
  step1.className = "step happiness show";
  step2.className = "step happiness__results";
};

// check for camerasupport
if (navigator.getUserMedia) {
  // set up stream

  var videoSelector = {video : true};
  if (window.navigator.appVersion.match(/Chrome\/(.*?) /)) {
    var chromeVersion = parseInt(window.navigator.appVersion.match(/Chrome\/(\d+)\./)[1], 10);
    if (chromeVersion < 20) {
      videoSelector = "video";
    }
  };

  navigator.getUserMedia(videoSelector, function( stream ) {
    if (vid.mozCaptureStream) {
      vid.mozSrcObject = stream;
    } else {
      vid.src = (window.URL && window.URL.createObjectURL(stream)) || stream;
    }
    vid.play();
  }, function() {
    //insertAltVideo(vid);
    alert("There was some problem trying to fetch video from your webcam. If you have a webcam, please make sure to accept when the browser asks for access to your webcam.");
  });
} else {
  //insertAltVideo(vid);
  alert("This demo depends on getUserMedia, which your browser does not seem to support. :(");
}

//vid.addEventListener('canplay', enablestart, false);startVideo
vid.addEventListener('canplay', startVideo, false);

/*********** setup of emotion detection *************/

var ctrack = new clm.tracker({useWebGL : true});
ctrack.init(pModel);

function startVideo() {
  // reset frames counter
  happinessFramesCounter = 0;
  // start video
  vid.play();
  // start tracking
  ctrack.start(vid);
  // start loop to draw face
  drawLoop();
}

function drawLoop() {

  var happinessValue = 0;

  happinessFramesCounter++;

  var cp = ctrack.getCurrentParameters();

  var er = ec.meanPredict(cp);
  if (er) {
    happinessValue = Math.ceil(er[3].value * 100);
  }

  if (happinessValue > happinessValueLimit) {
    happyIcon.className = 'emotion-icon show';
    unhappyIcon.className = 'emotion-icon';
  } else {
    unhappyIcon.className = 'emotion-icon show';
    happyIcon.className = 'emotion-icon';
  }

  if (happinessFramesCounter <= frameLimit ) {
    requestAnimFrame(drawLoop);
  } else {
    // happinessBtnShare

    if (happinessValue > happinessValueLimit) {
      console.log(">>>>>>>>>>>>", happinessValue + ' = YOU ARE HAPPY');
    } else {
      console.log(">>>>>>>>>>>>", happinessValue + ' = YOU ARE NOT HAPPY');
    }
    showResult(happinessValue);
  }
}

var ec = new emotionClassifier();
ec.init(emotionModel);
