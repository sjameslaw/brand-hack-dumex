$(document).ready(function() {


	var projectData,
		that = this,
		currentStep=0,
    currentStepid = 0,
    	smartness = 0,
    	$quiz_template,
    	currentQuestion,
  		quiz_data;

	loadConfing();

  function init() {
  	  //splash



    if(!localStorage.getItem('Dumex')) {
    	localStorage.setItem('Dumex', JSON.stringify({"user":{}, "currentStep" : "0"}));
    }

    var r =  JSON.parse(localStorage.getItem("Dumex"));
    var step = r.currentStep;

    currentStepid = step;
    currentStep = projectData.Dumex.steps[currentStepid];


	  if(step!= 0) {
      $('.splash').hide();
      $('.user-data').hide();
      loadStep(currentStep);

    } else {
      $('.splash').fadeIn(400).delay(800).fadeOut(400);
			$('.user-data').delay(1000).fadeIn(400);
      //User data
      $( ".user-data form" ).submit(function(e) {
        e.preventDefault();
        var inputs = $(e.target).find('input');
        var formData = {"user":{}};

        $.each(inputs, function (i, input) {
          if(input.type != 'submit'){
            formData['user'][input.name] = input.value;
          }
        });

        localStorage.setItem('Dumex', JSON.stringify(formData));

        $('.user-data').fadeOut(400);

        loadStep(currentStep);
      });
    }

  }

  function loadConfing() {
  	if(!projectData) {
	  	$.ajax({
		  dataType: "json",
		  url: "js/config.json",
		  success: function(response) {
		  	projectData = response;
		  	init();
		  }
		});
  	}
  };

  function loadStep(step) {
  	var $template = $('.user-select');
		$template.fadeIn();
  	$template.find('.user-select__title').text(step.name);
  	$template.find('.user-select__description').text(step.description);
  	$template.find('.user-select__continue a').attr('href', step.link);

  	$template.find('.step-select__item a#'+step.id).addClass('active');
    currentStepid = step.id;

    if(currentStepid < 2) {
      currentStepid ++;
    } else {
      currentStepid = 0;
    }


    var dumexStore = JSON.parse(localStorage.getItem('Dumex'));
    dumexStore.currentStep = currentStepid;
    localStorage.setItem('Dumex', JSON.stringify(dumexStore));
  };

    //Strengthness
  $('.smartness .smartness__cards__item a').on('click', function(e) {
    e.preventDefault();
    smartness++;

    if(smartness == 3) {
      smartness = 0;

      $('.smartness').fadeOut(400);
    }
  });
});
