$(document).ready(function() {
	var projectData,
		that = this,
    	smartness = 0,
    	$quiz_template,
    	currentQuestion,
  		quiz_data,
  		correct,
      shapeTimer,
      currentTime = 0,
      totalTime = 0,
  		results = 0;

	loadConfing();

  function loadConfing() {
  	if(!projectData) {
	  	$.ajax({
		  dataType: "json",
		  url: "js/config.json",
		  success: function(response) {
		  	projectData = response;
		  	startQuiz();
		  }
		});
  	}
  };

  function startCounter() {
     shapeTimer = setInterval(loopCounter, 1000);
  }

  function loopCounter() {
    currentTime++;

    var seconds = currentTime % 60;
    var minutes = Math.floor(currentTime / 60);
    var hours = Math.floor(minutes / 60);
    minutes %= 60;
    hours %= 60;

    if(seconds < 10) {
      seconds = "0"+seconds;
    }

    if(minutes < 10) {
      minutes = "0"+minutes;
    }

    if(hours < 10) {
      hours = "0"+hours;
    }
 
    $('.smartness__body__timer').html(hours+":"+minutes+":"+seconds);    
  }


  function startQuiz() {

  	quiz_data = projectData.Dumex.games[0].smartness[0].quiz;
  	$quiz_template = $('.smartness');
  	loadQuestion(0);
  };

  function loadQuestion(id) {
    currentTime = 0;
    startCounter();

    $('.smartness__body__timer').html("00:00:00"); 
  	$quiz_template.find('.smartness__question').text(quiz_data[id].question);
  	correct = quiz_data[id].correct;

  	$.each($quiz_template.find('.smartness__answers__cards a'), function(i,answer){
  		$(answer).find('img').attr('src', quiz_data[id].answer[i].img);
  		$(answer).attr('answer-id', i);
  	});
  };

    //Smartness
  $('.smartness__answers__cards a').on('click', function(e) {
    e.preventDefault();
    clearInterval(shapeTimer);

    if($(e.currentTarget).attr('answer-id') === correct.toString()) {
    	results ++;
    }

    totalTime += currentTime;
    smartness++;

    if(smartness == 3) {
      smartness = 0;

		  var dumexStore = JSON.parse(localStorage.getItem('Dumex'));
		  if (!dumexStore.results) {
		    dumexStore.results = {};
		  }

		  var perc = Math.ceil((results*100)/3);
		  dumexStore.results.smartness = perc;
		  localStorage.setItem('Dumex', JSON.stringify(dumexStore));


      var seconds = totalTime % 60;
      var minutes = Math.floor(totalTime / 60);

      if(seconds < 10) {
        seconds = "0"+seconds;
      }

      if(minutes < 10) {
        minutes = "0"+minutes;
      }

      $('.total-time').text("Total time:"+minutes+":"+seconds);

            if (dumexStore.results.smartness > 50) {
        $('.smartness__results').removeClass('error').addClass('success');
      } else {
        $('.smartness__results').removeClass('success').addClass('error');
      }

      $('.smartness').fadeOut(400);

    } else {
    	loadQuestion(smartness);
    }
  });


});
