$(document).ready(function() {
	var results = JSON.parse(localStorage.getItem("Dumex"));

    if(results.user.name) {
        $('.summary h1').text( results.user.name + "'s");
    }
	$('.smart').find('.result-icon-result').text(results.results.smartness+"%");
	$('.strength').find('.result-icon-result').text(results.results.strengthness+"%");
	$('.happinesss').find('.result-icon-result').text(results.results.happiness+"%");

});
