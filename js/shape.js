$(document).ready(function() {
	var projectData,
		that = this,
    	shape = 0,
    	$shape_template,
    	currentShape = 0,
  		shape_data,
  		correct,
      shapeTimer,
      currentTime = 0,
  		results = 0,
      totalTime = 0;

	loadConfing();

  function loadConfing() {
  	if(!projectData) {
	  	$.ajax({
		  dataType: "json",
		  url: "js/config.json",
		  success: function(response) {
		  	projectData = response;
		  	startShapes();
		  }
		});
  	}
  };

  /*function counter($el, n) {
      (function loop() {
         $el.html(n);
         if (n++) {
             shapeTimer = setTimeout(loop, 1000);
         }
      })();
  };*/

  function startCounter() {
     shapeTimer = setInterval(loopCounter, 1000);
  }

  function loopCounter() {
    currentTime++;

    var seconds = currentTime % 60;
    var minutes = Math.floor(currentTime / 60);
    var hours = Math.floor(minutes / 60);
    minutes %= 60;
    hours %= 60;

    if(seconds < 10) {
      seconds = "0"+seconds;
    }

    if(minutes < 10) {
      minutes = "0"+minutes;
    }

    if(hours < 10) {
      hours = "0"+hours;
    }

    $('.strengthness__body__timer').html(hours+":"+minutes+":"+seconds);
  }

  function startShapes() {
  	shape_data = projectData.Dumex.games[0].strengthness;
  	$shape_template = $('.strengthness');
  	loadShape(currentShape);
  };

  function loadShape(id) {
    currentTime = 0;
    $('.strengthness__body__timer').html("00:00:00");
  	$shape_template.find('.strengthness__description').text(shape_data[id].description);
    $shape_template.find('.strengthness__img img').attr('src', shape_data[id].img);
    startCounter();
  };

  $('a.stop').on('click', function(e){
        clearInterval(shapeTimer);
        if(currentShape < shape_data.length -1 ) {

          currentShape++;
          totalTime += currentTime;
          loadShape(currentShape);

        } else {

          $('.strengthness').fadeOut(400);

					var dumexStore = JSON.parse(localStorage.getItem('Dumex'));
          var perc = Math.ceil((totalTime*100)/5);
          dumexStore.results.strengthness = perc;

          var seconds = totalTime % 60;
          var minutes = Math.floor(totalTime / 60);

          if(seconds < 10) {
            seconds = "0"+seconds;
          }

          if(minutes < 10) {
            minutes = "0"+minutes;
          }

          $('.total-time').text("Total time:"+minutes+":"+seconds);

					if (dumexStore.results.strengthness < 50) {
						$('.strengthness__results').removeClass('error').addClass('success');
					} else {
						$('.strengthness__results').removeClass('success').addClass('error');
					}

					localStorage.setItem('Dumex', JSON.stringify(dumexStore));

        }
  });
});
